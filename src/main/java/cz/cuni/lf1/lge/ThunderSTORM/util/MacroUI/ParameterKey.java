package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI;

/**
 * A key to identify parameters in {@link ParameterTracker}.
 */
public abstract class ParameterKey {

    protected final java.lang.String name;
    protected final ParameterTracker values;

    ParameterKey(java.lang.String name, ParameterTracker values) {
        this.name = name;
        this.values = values;
    }

    public <T> T registerComponent(T component) {
        return values.registerComponent(this, component);
    }

    public Object getRegisteredComponent() {
        return values.getRegisteredComponent(this);
    }

    public static class Double extends ParameterKey {

        Double(java.lang.String name, ParameterTracker values) {
            super(name, values);
        }

        public double getValue() {
            return values.getDouble(this);
        }

        public void setValue(double value) {
            values.setDouble(this, value);
        }
    }

    public static class Integer extends ParameterKey {

        Integer(java.lang.String name, ParameterTracker values) {
            super(name, values);
        }

        public int getValue() {
            return values.getInt(this);
        }

        public void setValue(int value) {
            values.setInt(this, value);
        }
    }

    public static class String extends ParameterKey {

        String(java.lang.String name, ParameterTracker values) {
            super(name, values);
        }

        public java.lang.String getValue() {
            return values.getString(this);
        }

        public void setValue(java.lang.String value) {
            values.setString(this, value);
        }
    }

    public static class Boolean extends ParameterKey {

        Boolean(java.lang.String name, ParameterTracker values) {
            super(name, values);
        }

        public boolean getValue() {
            return values.getBoolean(this);
        }

        public void setValue(boolean value) {
            values.setBoolean(this, value);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ParameterKey)) {
            return false;
        }
        ParameterKey other = (ParameterKey) obj;
        return name.equals(other.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public java.lang.String toString() {
        return name;
    }
}
