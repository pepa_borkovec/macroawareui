package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI;

import cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.validators.ValidatorException;
import ij.IJ;
import ij.ImageListener;
import ij.ImagePlus;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.JTextComponent;

/**
 * A JDialog subclass that provides utility methods for simple creation of
 * dialogs that use {@link ParameterTracker}.
 * <p>
 * You have to override the {@link DialogStub#layoutComponents} method with
 * custom appearance of the dialog. The components used to get the parameter
 * values must be registered with the ParameterTracker using
 * {@code params.registerComponent(...)}. There are some protected helper
 * methods that implement often used functionality.
 * </p>
 * <p>
 * To show the dialog use {@link DialogStub#showAndGetResult()} and not the
 * methods inherited from JDialog.
 * </p>
 *
 * @author Josef Borkovec
 */
public abstract class DialogStub extends JDialog {

    protected ParameterTracker params;
    protected int result = JOptionPane.CLOSED_OPTION;
    private final Object callingThreadBlocker = new Object();

    public DialogStub(ParameterTracker params, Window owner, String title) {
        super(owner, title);
        this.params = params;
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        addCloseListener();
    }

    /**
     * Setups the dialog and shows it on screen. Blocks untill the dialog is
     * closed even for non modal dialogs.
     *
     * @return {@link JOptionPane#CLOSED_OPTION} if the dialog is closed,
     * {@link JOptionPane#CANCEL_OPTION} if cancel button is pressed,
     * {@link JOptionPane#OK_OPTION} if OK button is pressed
     * @throws RuntimeException if the dialog is not modal and this method is
     * called from EDT, or if current thread is interrupted while waiting
     */
    public int showAndGetResult() {
        setLayout(new GridBagLayout());
        setResizable(false);
        layoutComponents();
        pack();
        setVisible(true);
        if (!isModal()) {//modal dialogs block automatically
            if (SwingUtilities.isEventDispatchThread()) {
                throw new RuntimeException("Do not call this method from EDT. This is a possibly blocking call.");
            }
            try {
                //wait for the dialog to close
                while (isVisible()) {
                    synchronized (callingThreadBlocker) {
                        callingThreadBlocker.wait();
                    }
                }
            } catch (InterruptedException e) {
                throw new RuntimeException("Waiting for dialog interrupted.", e);
            }

        }
        return result;
    }

    /**
     * adds a listener to windowClosed events to unblock threads waiting for the
     * dialog
     */
    private void addCloseListener() {
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                if (!isModal()) {//modal dialogs handle thread blocking automatically
                    assert (!isVisible());
                    //wakeup threads waiting for dialog results
                    synchronized (callingThreadBlocker) {
                        callingThreadBlocker.notifyAll();
                    }
                }
            }
        });
    }

    /**
     * Creates a JPanel with buttons created by
     * {@link DialogStub#createDefaultsButton()}, {@link DialogStub#createOKButton()}
     * and {@link DialogStub#createCancelButton()}
     */
    protected JPanel createButtonsPanel() {
        JPanel buttons = new JPanel(new GridBagLayout());
        GridBagConstraints glueConstraints = new GridBagConstraints();
        glueConstraints.fill = GridBagConstraints.HORIZONTAL;
        glueConstraints.weightx = 1;

        buttons.add(createDefaultsButton());
        buttons.add(Box.createHorizontalGlue(), glueConstraints);
        buttons.add(createOKButton());
        buttons.add(createCancelButton());
        return buttons;
    }

    /**
     * Creates a JButton that does the following on click:
     * <ul>
     * <li>read values from dialog component</li>
     * <li>if validation fails, shows error message and returns</li>
     * <li>saves result of the dialog ({@link JOptionPane#OK_OPTION})</li>
     * <li>records macro options (if macro recorder is on)</li>
     * <li>saves values to preferences (if allowed)</li>
     * </ul>
     */
    protected JButton createOKButton() {
        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    params.readDialogOptions();
                    dispose();
                    result = JOptionPane.OK_OPTION;
                    params.recordMacroOptions();
                    if (params.isPrefsSavingEnabled()) {
                        params.savePrefs();
                    }
                } catch (ValidatorException ex) {
                    handleValidationException(ex);
                }
            }
        });
        this.getRootPane().setDefaultButton(okButton);
        return okButton;
    }

    /**
     * Creates a JButton that closes the dialog with result
     * {@link JOptionPane#CANCEL_OPTION}
     */
    protected JButton createCancelButton() {
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
                result = JOptionPane.CANCEL_OPTION;
            }
        });
        return cancelButton;
    }

    /**
     * Creates a JButton that resets the parametrs to their default values on
     * click.
     */
    protected JButton createDefaultsButton() {
        JButton defaultsButton = new JButton("Defaults");
        defaultsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                params.resetToDefaults(true);
            }
        });
        return defaultsButton;
    }

    /**
     * Creates a JComboBox with open images' titles as items. The items are
     * renewed when an image is opened or closed.
     *
     * @param addEmptyString specifies if an empty item shoud be added to the
     * item list
     */
    public static JComboBox<String> createOpenImagesComboBox(final boolean addEmptyString) {
        final JComboBox<String> cb = new JComboBox<String>(Utils.getOpenImageTitles(addEmptyString));
        ImagePlus.addImageListener(new ImageListener() {
            public void imageOpened(ImagePlus imp) {
                cb.setModel(new DefaultComboBoxModel<String>(Utils.getOpenImageTitles(addEmptyString)));
            }

            public void imageClosed(ImagePlus imp) {
                String[] openImageTitles = Utils.getOpenImageTitles(addEmptyString);
                if (openImageTitles == null) {
                    openImageTitles = new String[]{""};
                }
                cb.setModel(new DefaultComboBoxModel<String>(openImageTitles));
            }

            public void imageUpdated(ImagePlus imp) {
            }
        });
        return cb;
    }

    /**
     * Creates a JButton that shows a JFileChooser dialog and sets the path
     * chosen to the textField passed as argument
     *
     * @param pathTextField
     * @param compact if true uses a small button with "..." as text and with
     * minimal margin, else default margins are used and "Browse" is used as the
     * button text
     * @param fileFilters optional list of FileFilters to limit what files are
     */
    public static JButton createBrowseButton(final JTextComponent pathTextField, boolean compact, final FileFilter... fileFilters) {
        JButton browseButton = new JButton(compact ? "..." : "Browse");
        if (compact) {
            browseButton.setMargin(new Insets(1, 1, 1, 1));
        }
        browseButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser jfc = new JFileChooser(pathTextField.getText().isEmpty() ? null : pathTextField.getText());
                for (FileFilter ff : fileFilters) {
                    jfc.addChoosableFileFilter(ff);
                }
                if (fileFilters.length > 0) {
                    jfc.setFileFilter(fileFilters[0]);
                }
                jfc.addChoosableFileFilter(null);
                if (jfc.showDialog(null, "OK") == JFileChooser.APPROVE_OPTION) {
                    File file = jfc.getSelectedFile();
                    FileFilter ff = jfc.getFileFilter();
                    String path = file.getPath();
                    if (ff instanceof FileNameExtensionFilter) {
                        FileNameExtensionFilter fnef = (FileNameExtensionFilter) ff;

                        String nameLower = file.getName().toLowerCase();
                        String[] extensions = fnef.getExtensions();
                        boolean extOK = false;
                        for (String ext : extensions) { // check if it already has a valid extension
                            if (nameLower.endsWith('.' + ext.toLowerCase())) {
                                extOK = true;
                                break;
                            }
                        }
                        // if not, append the first one from the selected filter
                        if (!extOK) {
                            path = path + "." + extensions[0];
                        }
                    }
                    pathTextField.setText(path);
                }
            }
        });
        return browseButton;
    }

    /**
     * GridBagConstraints for the first column of a simple three column layout.
     *
     * @param row used for {@link GridBagConstraints#gridy}
     */
    protected GridBagConstraints firstColumn(int row) {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = row;
        c.weightx = 0.5;
        c.anchor = GridBagConstraints.LINE_END;
        c.insets = new Insets(0, 0, 0, 5);
        return c;
    }

    /**
     * GridBagConstraints for the second column of a simple three column layout.
     *
     * @param row used for {@link GridBagConstraints#gridy}
     */
    protected GridBagConstraints secondColumn(int row) {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = row;
        c.anchor = GridBagConstraints.WEST;
        c.insets = new Insets(1, 0, 1, 0);
        return c;
    }

    /**
     * GridBagConstraints for the third column of a simple three column layout.
     *
     * @param row used for {@link GridBagConstraints#gridy}
     */
    protected GridBagConstraints thirdColumn(int row) {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 2;
        c.gridy = row;
        c.weightx = 0.5;
        c.anchor = GridBagConstraints.WEST;
        c.insets = new Insets(0, 5, 0, 0);
        return c;
    }

    /**
     * GridBagConstraints for all three columns of a simple three column layout.
     *
     * @param row used for {@link GridBagConstraints#gridy}
     */
    protected GridBagConstraints threeColumns(int row) {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridwidth = 3;
        c.gridy = row;
        c.fill = GridBagConstraints.HORIZONTAL;
        return c;
    }

    /**
     * In this method you should create the dialog GUI components and add them
     * to the content pane. The components used to get the parameter values must
     * be registered with the ParameterTracker using
     * {@code params.registerComponent(...)}.
     */
    protected abstract void layoutComponents();

    /**
     * Method that handles validation errors. Default implementation shows a
     * dialog with the error message. Can be overrdiden to highlight which
     * component's validation failed. The source of the validation error can be
     * obtained using {@code ex.getSource()}.
     */
    protected void handleValidationException(ValidatorException ex) {
        IJ.showMessage("Input validation error", ex.getMessage());
    }
}
