package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.handlers;

import cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.validators.ValidatorException;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Josef Borkovec <josef.borkovec[at]lf1.cuni.cz>
 */
class JTextComponentToDoubleHandler implements ComponentHandler<Double> {

    public Double getValueFromComponent(Object comp) {
        assert (comp instanceof JTextComponent);
        JTextComponent textField = (JTextComponent) comp;
        try {
            return Double.parseDouble(textField.getText());
        } catch (NumberFormatException e) {
            throw new ValidatorException("The input string \"" + textField.getText() + "\" cannot be converted to double.", e, textField);
        }
    }

    public void setValueToComponent(Double value, Object comp) {
        assert (comp instanceof JTextComponent);
        JTextComponent textField = (JTextComponent) comp;
        textField.setText(value + "");
    }
}
