package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.validators;

/**
 * Factory that provides several validators for integer parameters.
 */
public class IntegerValidatorFactory {

    /**
     * A validator that checks if a value is in range. Border values included.
     */
    public static Validator<Integer> rangeInclusive(final int low, final int high) {
        return new Validator<Integer>() {
            public void validate(Integer input) throws ValidatorException {
                checkLowerBoundInclusive(low, input);
                checkUpperBoundInclusive(high, input);
            }
        };
    }

    /**
     * A validator that checks if a value is in range. Border values are not
     * included.
     */
    public static Validator<Integer> rangeExclusive(final int low, final int high) {
        return new Validator<Integer>() {
            public void validate(Integer input) throws ValidatorException {
                checkLowerBoundExclusive(low, input);
                checkUpperBoundExclusive(high, input);
            }
        };
    }

    /**
     * A validator that checks if a value is positive. Zero included.
     */
    public static Validator<Integer> positive() {
        return new Validator<Integer>() {
            public void validate(Integer input) throws ValidatorException {
                checkLowerBoundInclusive(0, input);
            }
        };
    }

    /**
     * A validator that checks if a value is positive. Zero not included.
     */
    public static Validator<Integer> positiveNonZero() {
        return new Validator<Integer>() {
            public void validate(Integer input) throws ValidatorException {
                checkLowerBoundExclusive(0, input);
            }
        };
    }

    /**
     * A validator that checks if a value is negative. Zero included.
     */
    public static Validator<Integer> negative() {
        return new Validator<Integer>() {
            public void validate(Integer input) throws ValidatorException {
                checkUpperBoundInclusive(0, input);
            }
        };
    }

    static void checkLowerBoundInclusive(int bound, int value) {
        if (value < bound) {
            throw new ValidatorException("Input value must be greater than or equal to " + bound + ". Input value: " + value);
        }
    }

    static void checkLowerBoundExclusive(int bound, int value) {
        if (value <= bound) {
            throw new ValidatorException("Input value must be greater than " + bound + ". Input value: " + value);
        }
    }

    static void checkUpperBoundInclusive(int bound, int value) {
        if (value > bound) {
            throw new ValidatorException("Input value must be smaller than or equal to " + bound + ". Input value: " + value);
        }
    }

    static void checkUpperBoundExclusive(int bound, int value) {
        if (value >= bound) {
            throw new ValidatorException("Input value must be smaller than " + bound + ". Input value: " + value);
        }
    }
}
