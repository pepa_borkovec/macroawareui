package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.handlers;

import javax.swing.text.JTextComponent;

/**
 *
 * @author Josef Borkovec <josef.borkovec[at]lf1.cuni.cz>
 */
class JTextComponentToStringHandler implements ComponentHandler<String> {

    public String getValueFromComponent(Object comp) {
        assert (comp instanceof JTextComponent);
        JTextComponent textField = (JTextComponent) comp;
        return textField.getText();
    }

    public void setValueToComponent(String value, Object comp) {
        assert (comp instanceof JTextComponent);
        JTextComponent textField = (JTextComponent) comp;
        textField.setText(value);
    }
}
