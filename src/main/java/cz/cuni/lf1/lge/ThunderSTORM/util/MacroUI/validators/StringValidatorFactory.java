package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.validators;

import cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.Utils;
import java.io.File;
import java.util.Arrays;

/**
 * Factory that provides several validators for String parameters.
 */
public class StringValidatorFactory {

    /**
     * A validator that checks if a String value is one of possible String
     * values.
     *
     * @param possibleValues
     */
    public static Validator<String> isMember(final String[] possibleValues) {
        return new Validator<String>() {
            public void validate(String input) throws ValidatorException {
                for (String possibleValue : possibleValues) {
                    if (input == null && possibleValue == null) {
                        return;
                    }
                    if (input != null && input.equals(possibleValue)) {
                        return;
                    }
                }
                throw new ValidatorException("The value \"" + input + "\" is not one of the possible values: " + Arrays.toString(possibleValues));
            }
        };
    }

    /**
     * A validator that checks if a String mathes a regular expression.
     *
     * @param pattern see {@link String#matches(String)}
     * @param requestedSyntaxMessage optional String with human readable pattern
     * specification for better error message.
     */
    public static Validator<String> regularExpression(final String pattern, final String requestedSyntaxMessage) {
        return new Validator<String>() {
            public void validate(String input) throws ValidatorException {
                if (pattern == null) {
                    throw new ValidatorException("The input string cannot be null.");
                }
                if (!input.matches(pattern)) {
                    StringBuilder errMessage = new StringBuilder();
                    errMessage.append("The input string \"");
                    errMessage.append(input);
                    errMessage.append("\" does not match the requested format");
                    if (requestedSyntaxMessage != null) {
                        errMessage.append(": ");
                        errMessage.append(requestedSyntaxMessage);
                    }
                    errMessage.append(".");
                    throw new ValidatorException(errMessage.toString());
                }
            }
        };
    }

    /**
     * A validator that checks if a String is equal to one of the titles of open
     * ImageJ images.
     *
     * @param allowEmpty true if empty String should be considered a valid
     * option
     */
    public static Validator<String> openImages(final boolean allowEmpty) {
        return new Validator<String>() {
            public void validate(String input) throws ValidatorException {

                String[] possibleValues = Utils.getOpenImageTitles(allowEmpty);
                if (possibleValues != null) {
                    try {
                        isMember(possibleValues).validate(input);
                    } catch (ValidatorException ex) {
                        throw new ValidatorException("The value \"" + input + "\" is not one of the open image titles.");
                    }
                } else {
                    throw new ValidatorException("The value \"" + input + "\" is not an open image title. No images are open.");
                }
            }
        };
    }

    /**
     * A validator that checks if the String is a valid path to an existing
     * file.
     */
    public static Validator<String> fileExists() {
        return new Validator<String>() {
            public void validate(String input) throws ValidatorException {
                File f = new File(input);
                if (!f.exists()) {
                    throw new ValidatorException("The file \"" + input + "\" does not exist.");
                }
                if (!f.isFile()) {
                    throw new ValidatorException("\"" + input + "\" is not a file.");
                }
            }
        };
    }

    /**
     * A validator that checks if the String is a valid path to an existing
     * directory.
     */
    public static Validator<String> directoryExists() {
        return new Validator<String>() {
            public void validate(String input) throws ValidatorException {
                File f = new File(input);
                if (!f.exists()) {
                    throw new ValidatorException("The file \"" + input + "\" does not exist.");
                }
                if (!f.isDirectory()) {
                    throw new ValidatorException("\"" + input + "\" is not a directory.");
                }
            }
        };
    }
}
