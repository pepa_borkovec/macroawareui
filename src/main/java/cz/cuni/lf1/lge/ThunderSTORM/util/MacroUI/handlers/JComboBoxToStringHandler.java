package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.handlers;

import javax.swing.JComboBox;

/**
 *
 * @author Josef Borkovec <josef.borkovec[at]lf1.cuni.cz>
 */
class JComboBoxToStringHandler implements ComponentHandler<String> {

    public String getValueFromComponent(Object comp) {
        assert (comp instanceof JComboBox);
        JComboBox<String> cb = (JComboBox<String>) comp;

        return (String) cb.getSelectedItem();
    }

    public void setValueToComponent(String value, Object comp) {
        assert (comp instanceof JComboBox);
        JComboBox<String> cb = (JComboBox<String>) comp;

        cb.setSelectedItem(value);
    }
}
