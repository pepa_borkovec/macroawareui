package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.handlers;

/**
 * A handler that is used to manipulate GUI components. Setting a value and
 * reading it back is done through this interface. Support for more GUI
 * components can be added by creating new handler for it and adding it to the
 * {@link ComponentHandlerMap}.
 *
 * @author Josef Borkovec
 */
public interface ComponentHandler<T> {

    /**
     * Retrieve the parameter value from the GUI component.
     *
     * @param comp the component that was registered for the parameter
     * @return the parameter value
     */
    public T getValueFromComponent(Object comp);

    /**
     * Update the state of the GUI component to reflect the value of the
     * parameter.
     *
     * @param value the value of the parameter
     * @param comp the component registered for the prameter
     */
    public void setValueToComponent(T value, Object comp);

}
