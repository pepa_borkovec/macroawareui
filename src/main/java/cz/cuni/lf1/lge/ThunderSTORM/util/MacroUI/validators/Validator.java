package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.validators;

/**
 * Does input validation of parameters.
 */
public interface Validator<T> {

    /**
     * Validate the input.
     *
     * @param input
     * @throws ValidatorException when the validation fails.
     */
    public void validate(T input) throws ValidatorException;
}
