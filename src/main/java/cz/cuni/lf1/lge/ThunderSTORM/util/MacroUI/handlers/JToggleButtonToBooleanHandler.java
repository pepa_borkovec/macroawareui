package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.handlers;

import cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.Utils;
import javax.swing.JToggleButton;

/**
 *
 * @author Josef Borkovec <josef.borkovec[at]lf1.cuni.cz>
 */
class JToggleButtonToBooleanHandler implements ComponentHandler<Boolean> {

    public Boolean getValueFromComponent(Object comp) {
        assert (comp instanceof JToggleButton);
        JToggleButton button = (JToggleButton) comp;

        return button.isSelected();
    }

    public void setValueToComponent(Boolean value, Object comp) {
        assert (comp instanceof JToggleButton);
        JToggleButton button = (JToggleButton) comp;

        if (button.isSelected() != value) {
            Utils.click(button);
        }
    }

}
