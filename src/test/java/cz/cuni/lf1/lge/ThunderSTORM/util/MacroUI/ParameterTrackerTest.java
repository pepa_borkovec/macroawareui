package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.validators.ValidatorException;
import cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.validators.Validator;
import ij.Macro;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import junit.framework.TestCase;
import org.junit.Test;

/**
 *
 * @author Josef Borkovec <josef.borkovec[at]lf1.cuni.cz>
 */
public class ParameterTrackerTest extends TestCase {

    public void testDoubleFieldMacro() {
        ParameterTracker gp = new ParameterTracker();
        ParameterKey.Double d1 = gp.createDoubleField("d1", null, 0);
        Thread.currentThread().setName("Run$_TEST");
        Macro.setOptions("d1=5");
        gp.readMacroOptions();
        assertEquals(5.0, gp.getDouble(d1), 1e-10);
    }

    public void testIntFieldMacro() {
        ParameterTracker gp = new ParameterTracker();
        ParameterKey.Integer i1 = gp.createIntField("i1", null, 0);
        Thread.currentThread().setName("Run$_TEST");
        Macro.setOptions("i1=5");
        gp.readMacroOptions();
        assertEquals(5, gp.getInt(i1));
    }

    public void testDoubleFieldComponent() {
        ParameterTracker gp = new ParameterTracker();
        ParameterKey.Double d1 = gp.createDoubleField("d1", null, 0);
        JTextField textField = new JTextField("5");
        gp.registerComponent(d1, textField);
        gp.readDialogOptions();
        assertEquals(5.0, gp.getDouble(d1), 1e-10);
    }

    @Test
    public void testDoubleFieldComponentValidator() {
        ParameterTracker gp = new ParameterTracker();
        ParameterKey.Double d1 = gp.createDoubleField("d1", new Validator<Double>() {
            public void validate(Double input) throws ValidatorException {
                throw new ValidatorException("Got Ya!");
            }
        }, 0);
        JTextField textField = new JTextField("5");
        gp.registerComponent(d1, textField);

        try {
            gp.readDialogOptions();
            fail("expected exception");
        } catch (ValidatorException e) {
            assertEquals(textField, e.getSource());
        }
    }

    @Test
    public void testChoiceComboBox() {
        ParameterTracker gp = new ParameterTracker();
        String[] items = {"first", "second", "third"};
        ParameterKey.String ch1 = gp.createStringField("ch1", null, items[0]);
        JComboBox<String> cb = new JComboBox<String>(items);
        gp.registerComponent(ch1, cb);

        cb.setSelectedItem(items[2]);
        gp.readDialogOptions();
        assertEquals(items[2], ch1.getValue());
    }

    @Test
    public void testChoiceRadioGroup() {
        ParameterTracker gp = new ParameterTracker();
        String[] items = {"first", "second", "third"};
        ParameterKey.String ch1 = gp.createStringField("ch1", null, items[0]);
        JRadioButton radioButton0 = new JRadioButton(items[0], true);
        JRadioButton radioButton1 = new JRadioButton(items[1]);
        JRadioButton radioButton2 = new JRadioButton(items[2]);

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(radioButton2);
        buttonGroup.add(radioButton1);
        buttonGroup.add(radioButton0);

        radioButton2.setSelected(true);
        gp.registerComponent(ch1, buttonGroup);

        radioButton2.doClick();
        gp.readDialogOptions();

        assertEquals(items[2], ch1.getValue());
    }

    @Test
    public void testChoiceJTabbedPane() {
        ParameterTracker gp = new ParameterTracker();
        String[] items = {"first", "second", "third"};
        ParameterKey.String tab = gp.createStringField("tab", null, items[2]);

        JTabbedPane pane = new JTabbedPane();
        pane.add(items[0], new JPanel());
        pane.add(items[1], new JPanel());
        pane.add(items[2], new JPanel());

        tab.registerComponent(pane);

        pane.setSelectedIndex(0);
        gp.resetToDefaults(true);
        assertEquals(items[2], pane.getTitleAt(pane.getSelectedIndex()));

        pane.setSelectedIndex(1);
        gp.readDialogOptions();
        assertEquals(items[1], tab.getValue());
    }

    public void testNoGUIPars() {
        ParameterTracker pars = new ParameterTracker();
        ParameterKey.Integer i1 = pars.createIntField("i1", null, 0);
        ParameterKey.Integer i2 = pars.createIntField("i2", null, 0);
        JTextField textField = new JTextField("5");
        pars.registerComponent(i1, textField);
        
        //reading dialog options
        try{
            pars.readDialogOptions();
            fail("Parameter i2 does not have any component registered. Should have thrown exception.");
        }catch(NullPointerException ex){}
        
        //reading dialog options when nogui pars are allowed
        pars.setNoGuiParametersAllowed(true);
        pars.readDialogOptions();
        assertEquals(5, i1.getValue());
        assertEquals(0, i2.getValue()); // default value
        
        //reading macro options
        Thread.currentThread().setName("Run$_TEST");
        Macro.setOptions("i1=5 i2=10");
        pars.readMacroOptions();
        assertEquals(5, i1.getValue());
        assertEquals(10, i2.getValue());
        
        
    }
    
    @Test
    public void testGsonDump() {
        ParameterTracker gp = new ParameterTracker();
        String[] items = {"first", "second", "third"};
        ParameterKey.String ch1 = gp.createStringField("ch1", null, items[0]);
        JComboBox<String> cb = new JComboBox<String>(items);
        gp.registerComponent(ch1, cb);

        cb.setSelectedItem(items[2]);

        ParameterKey.Double d1 = gp.createDoubleField("d1", null, 0);
        JTextField textField = new JTextField("5");
        gp.registerComponent(d1, textField);

        gp.readDialogOptions();

        Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
        System.out.println(gson.toJson(gp));
    }

}
